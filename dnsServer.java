/**
* DNS Server
* CSE 310 - Fall 2015
* @author Terrell Mack, Shakeeb Saleh
 */

import java.io.*; // Provides for system input and output through data
                  // streams, serialization and the file system
import java.net.*; // Provides the classes for implementing networking
                   // applications

import java.util.ArrayList;
import java.util.Arrays;

// TCP Server Class
class DNSServer {

      private static ArrayList<Record> records; //holds database of records

        public static void main(String argv[]) throws Exception {
          records = new ArrayList<Record>();
                // TODO: remove (FOR DEBUGGING)
                // get the port number assigned from the command line
                //int lisPort = Integer.parseInt(argv[0]);
                //ServerSocket welcomeSocket = new ServerSocket(lisPort);

                // create a server socket (TCP) eph
                ServerSocket welcomeSocket = new ServerSocket(0);

                System.out.print("\nDNS server ready on port " + welcomeSocket.getLocalPort());

                // loop infinitely (process clients sequentially)
              while(true) {
                        // Wait and accept client connection
                        Socket connectionSocket = welcomeSocket.accept();

                        // Each connection gets its own thread so multiple can run at once
                        ClientThread ct = new ClientThread(connectionSocket, records);
                        ct.start();
                }
        }
}

class ClientThread extends Thread {

  private static ArrayList<Record> records;
  Socket connectionSocket;
  String clientMessage;
  String serverResponse;
  ArrayList<String> clientArgs;

  ClientThread(Socket connSock, ArrayList<Record> recs) {
    connectionSocket = connSock;
    records = recs;
  }

  // handles the client request
   public void run () {
     try {
     //create an input stream from the socket input stream
     BufferedReader inFromClient = new BufferedReader(
        new InputStreamReader(connectionSocket.getInputStream()));

     // create an output stream from the socket output stream
     DataOutputStream outToClient =
     new DataOutputStream(connectionSocket.getOutputStream());

     // read a line form the input stream
     clientMessage = inFromClient.readLine();

     // convert the client message into arguments
     clientArgs = convertMessage(clientMessage);

     // figures out what to do with the arguements
     serverResponse = parseArgs(clientArgs);

     // send the capitalized sentence back to the  client
     outToClient.writeBytes(serverResponse);

     // close the connection socket
     connectionSocket.close();
   } catch(IOException e) {
     System.out.print("Error: " + e);
   }
   }

   // converts a string to a list of arguments
   public static ArrayList<String> convertMessage(String message) {
     ArrayList<String> args = null;

       if(message.length() == 0){
           return args;
       }

       String[] intermediary;
       intermediary = message.split(" "); // splits message by white space
       args = new ArrayList<String>(Arrays.asList(intermediary));
       return args;
   }

   // figure out what to do using a list of arguments
   public static String parseArgs(ArrayList<String> args) {

     // if the list is empty
     if(args == null || args.size() == 0) {
       return "No arguments from Client";
     }

     // put argument
     if(args.get(0).equalsIgnoreCase("put")) {
       if(args.size() != 4)
         return "Wrong number of arguments for put";
       return put(args.get(1), args.get(2), args.get(3));
     }

     // get argument
     else if(args.get(0).equalsIgnoreCase("get")) {
       if(args.size() != 3)
         return "Wrong number of arguments for get";
       Record rec = get(args.get(1), args.get(2));
       return rec == null ? "Record Not Found" : get(args.get(1), args.get(2)).toString();

     }

     // delete argumenet
     else if(args.get(0).equalsIgnoreCase("del")) {
       if(args.size() != 3)
         return "Wrong number of arguments for del";
       return del(args.get(1), args.get(2));
     }

     // browse arguement
     else if(args.get(0).equalsIgnoreCase("browse")) {
       String recs = browse();
       return recs == null ? "Records are Empty" : recs;
     }

     // if none of the expected arguments were used
     else {
       return "Invalid Argument:" + args.get(0);
     }

   }

   // adds a new record from the records database
   public static String put(String name, String value, String type){
       if(!(type.equalsIgnoreCase("NS")
               || type.equalsIgnoreCase("A"))){ // add other types when neccessary
           return "Invalid Type for Put: " + type;
       }

       // TODO if record already exists than update
       Record record = new Record(name, value, type);
       records.add(record);
       return "Succesfully Added Record " + value;
   }

   // gets a record from the records database
   public static Record get(String name, String type){
       // if there arent exactly 3 arguments, its wrong
       // first argument is get itself
       // must be in order name, type

       // returns a Record with that name and type
       // search by type, and then query name
       Record result = findRecord(name, type);
       if(result == null){
           System.out.println("record of name:" + name + "and Type" + type + "does not exist.");
           return null;
       }
       return result;
   }

   // deletes a record from the record database
   public static String del(String name, String type) {
       Record result = findRecord(name, type);
       if(result == null){
           return "record of name:" + name + "and Type" + type + "does not exist.";
       } else {
           records.remove(result);
           return "Record Deleted Succesfully";
       }
   }

   // returns all records from the record databse
   public static String browse() {
       if(records.isEmpty()){
           return "Database is empty";
       } else {
           //iterate through database,
           // putting everything into a massive string
           // and making sure there are endls after each record
         String allRecords = new String();
           for(Record r: records){
               allRecords += r.toString() + "+";
           }
           return allRecords;
       }
   }

   // find a particular record from the record database
   public static Record findRecord(String name, String type) {
     for(Record r : records){
         if(r.getType().equals(type)){
             if(r.getName().equals(name)){
                 return r;
             }
         }
     }
     return null;
   }
}
