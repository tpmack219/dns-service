/**
* A modified version of DNSServer.java for specific types
* CSE 310 - Fall 2015
* @author Terrell Mack, Shakeeb Saleh
 */

import java.io.*; // Provides for system input and output through data
                  // streams, serialization and the file system
import java.net.*; // Provides the classes for implementing networking
                   // applications

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
// TCP Server Class

class ManagerTypeServer {

  public static void main(String[] args) {

    TypeServer ts = new TypeServer(args[0]);
    try {
    ts.start();
    System.out.println(ts.getPort());
    }   catch(Exception e) {

      System.out.println("Error: " + e);
    }
  }

}

class TypeServer {

      private String type;
      private ServerSocket welcomeSocket;
      private ArrayList<Record> records; //holds database of records

      public TypeServer(String type) {
        this.type = type;
      }

      public void start() throws Exception {
        records = new ArrayList<Record>();

              // create a server socket (TCP) eph
              welcomeSocket = new ServerSocket(0);
            //  ServerSocket welcomeSocket2 = new ServerSocket(0);
          //    System.out.println(getPort());
          System.out.println(welcomeSocket.getLocalPort());
              // loop infinitely (process clients sequentially)
            while(true) {
                      // Wait and accept client connection
                      Socket connectionSocket = welcomeSocket.accept();

                      // Each connection gets its own thread so multiple can run at once
                      ClientThread ct = new ClientThread(connectionSocket, records, type);
                      ct.start();
              }

        }

        public String getType() {
          return type;
        }

        public int getPort() {
          return welcomeSocket.getLocalPort();
        }

      // TODO create end function
}

class ClientThread extends Thread {

  private static ArrayList<Record> records;
  Socket connectionSocket;
  String clientMessage;
  String serverResponse;
  ArrayList<String> clientArgs;
  static String type;
  public static HashMap<Integer, Integer> numbers; // matches a response code to it's acknowledgement response code
  public static HashMap<Integer, String> acks; //maps an integer to it's cooresponding acknowledgement

  ClientThread(Socket connSock, ArrayList<Record> recs, String type) {
    connectionSocket = connSock;
    records = recs;
    this.type = type;
  }

  // handles the client request
   public void run() {
     try {
     //create an input stream from the socket input stream
     initHashMaps();
     BufferedReader inFromClient = new BufferedReader(
        new InputStreamReader(connectionSocket.getInputStream()));

     // create an output stream from the socket output stream
     DataOutputStream outToClient =
     new DataOutputStream(connectionSocket.getOutputStream());

     // read a line form the input stream
     clientMessage = inFromClient.readLine();
     //System.out.print(clientMessage);

     // convert the client message into arguments
     clientArgs = convertMessage(clientMessage);

     // figures out what to do with the arguements
     serverResponse = parseArgs(clientArgs);

     // send response back to client
     outToClient.writeBytes(serverResponse);

     // close the connection socket
     connectionSocket.close();
   } catch(IOException e) {
     System.out.print("Error: " + e);
   }
   }

   // converts a string to a list of arguments
   public static ArrayList<String> convertMessage(String message) {
      ArrayList<String> args = new ArrayList<String>();
      System.out.println("Recieved from Client:"+ message);
       if(message.length() == 0){
           return args;
       }
       String[] cont = message.split("#", 2); //split in two on hashtag for header info and message content
       String[] headerPieces = cont[0].split(" ", 2);
       args.add(headerPieces[0]);// add the number as the first argument to the list of arguments

       String[] intermediary;
       intermediary = cont[1].split(" "); // splits message by white space
       ArrayList<String> intermediary2  = new ArrayList<String>(Arrays.asList(intermediary));
       args.addAll(intermediary2);
       // now this has become an number, followed by a list of arguments
       System.out.println(args.toString());
       return args;
   }

   // figure out what to do using a list of arguments
   public static String parseArgs(ArrayList<String> args) {
     // i've gotta change this whole beautiful thing, sadly
     // if the list is empty
     if(args == null || args.size() == 0) {
       return "21 ERR#No arguments from Client";
     }

     System.out.println(args.get(0));
     int responseCode = Integer.parseInt(args.get(0)); //get the responseCode from the args,
     // the rest of the args are treated the same way as they were before
     String result = "";
     System.out.println("parsing args");

     switch(responseCode){
       case 1: // put
          System.out.println("Response Code:"+ responseCode);
          if(args.size() != 3){ // put <name> <value>, type is already defined
            System.out.println("wrong number of arguments");
            result = "Wrong number of arguments for put"; // error code
            // add on header error info
            result = "13 ERR#" + result;
            System.out.println("Sending to Client:"+result);
          } else {
            System.out.println("correct number of arguments");
            result = put(args.get(1), args.get(2), type);
            // add on header info
            result = "2 TUP#" + result;
            System.out.println("Sending to Client:"+result);
          }
          return result;
       case 3: // get
          System.out.println("Response Code:"+ responseCode);
          if(args.size() != 2){ //get <name>, type is already defined
            result = "Wrong number of arguments for get"; // error code
            result = "14 ERR#" + result;
            System.out.println("Sending to Client:"+result);
            return result;
          }
          Record rec = get(args.get(1), type);
          if(rec == null){
            result = "Record Not Found"; // error code
            result = "15 ERR#" + result;
            System.out.println("Sending to Client:"+result);
          } else {
            result = rec.toString();
            result = "4 TEG#" + result;
            System.out.println("Sending to Client:"+result);
          }
          return result;
       case 5: // del
          System.out.println("Response Code: "+ responseCode);
          if(args.size() != 2){ // del <name> , type is already specified
              result = "Wrong number of arguments for del";
              result = "16 ERR#" + result;
              System.out.println("Sending to Client: "+result);
              return result;
            } else {
              result = del(args.get(1), type);
              // header for DEL is handled in the function itself
              System.out.println("Sending to Client: "+result);
              return result;
            }
       case 7: // browse
          System.out.println("Response Code: "+ responseCode);
          result = browse();
          if(result == null){
            result = "Records are Empty";
            result = "18 ERR#" + result;
            System.out.println("Sending to Client: "+result);
          } else {
            result = "8 ORB#" + result;
            System.out.println("Sending to Client: "+result);
          }
          return result;
       case 9: // type
          // i actually have no idea how to handle this
          System.out.println("Response Code: "+ responseCode);
          result = "10 PYT#Type is currently: "+ type +" if you wish to switch to a new type, issue the 'done' command";
          System.out.println("Sending to Client: "+result);
          return result;
       case 11:// done
       // this one just ends up being an exit
       // it never reaches here
          System.out.println("Response Code: "+ responseCode);
          result = "11 NOD# ";
          System.out.println("Sending to Client:"+result);
          return result;
      default: // shouldn't be able to get here
          System.out.println("Response Code: "+ responseCode);
          result = "19 ERR# default";
          System.out.println("Sending to Client: "+result);
          return result;

     }
   }

   // adds a new record from the records database
   public static String put(String name, String value, String type){
       //global type variable means that it can be any type
       if (findRecord(name, type) != null){
         Record r = (findRecord(name, type));
         r.setValue(value); // only thing that really changes is value,
         return "Succesfully Modified Record " + name;
       }
       Record record = new Record(name, value, type);
       records.add(record);
       return "Succesfully Added Record " + value;
   }

   // gets a record from the records database
   public static Record get(String name, String type){
       // if there arent exactly 2 arguments
       // first argument is get itself
       // must be in order name, type

       // returns a Record with that name and type
       // search by type, and then query name
       Record result = findRecord(name, type);
       if(result == null){
           System.out.println("record of name: " + name + " and Type " + type + " does not exist.");
           return null;
       }
       return result;
   }

   // deletes a record from the record database
   public static String del(String name, String type) {
       Record result = findRecord(name, type);
       String returner;
       // for del the response code goes straight in here
       if(result == null){
           returner = "record of name: " + name + " and Type " + type + " does not exist.";
           returner = "17 ERR#" + returner;
           return returner;
       } else {
           records.remove(result);
           returner = "Record Deleted Succesfully";
           returner = "6 LED#"+ returner;
           return returner;
       }
   }

   // returns all records from the record databse
   public static String browse() {
       if(records.isEmpty()){
           return "Database is empty";
       } else {
           //iterate through database,
           // putting everything into a massive string
           // and making sure there are endls after each record
         String allRecords = new String();
           for(Record r: records){
               allRecords += r.toString() + "+";
           }
           return allRecords;
       }
   }

   // find a particular record from the record database
   public static Record findRecord(String name, String type) {
     for(Record r : records){
         if(r.getType().equals(type)){
             if(r.getName().equals(name)){
                 return r;
             }
         }
     }
     return null;
   }

   public static void initHashMaps(){
     // initalizes the Hashmaps
     numbers = new HashMap<Integer, Integer>();
     // numbers maps response code values to the response code values for acks
     numbers.put(1,2); // put, 1 maps to 2
     numbers.put(3,4); // get 3 maps to 4
     numbers.put(5,6); // del 5 maps to 6
     numbers.put(7,8); // browse 7 maps to 8
     numbers.put(9,10); // type 9 maps to 10
     numbers.put(11,12); //  done 11 maps to 12
     // all errors have response codeds higher than 12
     acks = new HashMap<Integer, String>();
     // maps a response code to the appropriate ack
     acks.put(1, "TUP");
     acks.put(3, "TEG");
     acks.put(5, "LED");
     acks.put(7, "ORB");
     acks.put(9, "PYT");
     acks.put(11,"NOD");
   }

}
