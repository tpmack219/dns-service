/**
 *
 * @author Terrell Mack, Shakeeb Saleh
 */

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.nio.file.Files;

// TCP Server Class
class Manager {

  private static Socket connectionSocket;
  private static String clientMessage;
  private static String serverResponse;
  private static boolean serversStarted = false;
  private static ArrayList<String> clientArgs;

  private static ArrayList<String> serverTypes;
  private static ArrayList<Integer> serverPorts;

  private static String typesFilePath = "manager.in";
  static int wellKnownPort = 4132;

  public static void main(String[] args) {
    // generate servers types based on type file
    loadServersFrom(typesFilePath);

    // startup all the servers
    startServers();
    serverInfo();
    try {
      // create a server socket (TCP)
      ServerSocket welcomeSocket = new ServerSocket(wellKnownPort);

      while(true) {
        // Wait and accept client connection
        Socket connectionSocket = welcomeSocket.accept();

        // Each manager gets its own thread so multiple can run at once
        ConnectionThread ct = new ConnectionThread(connectionSocket, serverTypes, serverPorts);
        ct.start();
    }
  } catch(IOException e) {
    System.out.print("Error: " + e);
  }}

  // starts up Type Servers, each on their own process
  public static void startServers() {
    serverPorts = new ArrayList<Integer>();
    if(serversStarted == false) {
      // Each server gets its own process
      for(String s : serverTypes) {
        try {
        ProcessBuilder pb = new ProcessBuilder("java","ManagerTypeServer", s);
        Process proc = pb.start();
        InputStreamReader input = new InputStreamReader(proc.getInputStream());
        BufferedReader br = new BufferedReader(input);
        String portNumber = br.readLine();
        serverPorts.add(Integer.parseInt(portNumber));
        // after eating input from the process, how do i redirect it to stdout?
        // this could always be it's own thread, that blocks on input from the server, and just prints it out
        // pass the buffered reader as an argument
        ServerPrintThread spt = new ServerPrintThread(br);
        spt.start();

      } catch(Exception e) {
        System.out.println("Error: " + e);
      }
      }
    }
  }

  // this prints out all the type servers with the port numbers they are running on
  public static void serverInfo() {

    System.out.println("Server Information: ");
    for(int i = 0; i < serverTypes.size(); i++) {
      System.out.print(serverTypes.get(i));
      System.out.print(" " + serverPorts.get(i) + "\n");
    }

  }

  public static void loadServersFrom(String filename) {
    serverTypes = new ArrayList<String>();
    try {
    BufferedReader br = new BufferedReader(new FileReader(filename));
    String type = br.readLine();
    while (type != null) {
        serverTypes.add(type);
        type = br.readLine();
    }
    br.close();
    } catch(Exception e) {
      System.out.println("Error:" + e);
    }
  }
}

class ConnectionThread extends Thread {

  private static Socket connectionSocket;
  private static String clientMessage;
  private static String serverResponse;
  private static boolean serversStarted = false;
  private static ArrayList<String> clientArgs;

  private static ArrayList<String> serverTypes;
  private static ArrayList<Integer> serverPorts;

  ConnectionThread(Socket connSock, ArrayList<String> types, ArrayList<Integer> ports) {
    connectionSocket = connSock;
    serverTypes = types;
    serverPorts = ports;
  }

  // handles the client request
   public void run () {
     try {
       //create an input stream from the socket input stream
       BufferedReader inFromClient = new BufferedReader(
          new InputStreamReader(connectionSocket.getInputStream()));

       // create an output stream from the socket output stream
       DataOutputStream outToClient =
          new DataOutputStream(connectionSocket.getOutputStream());

       // read a line form the input stream
       clientMessage = inFromClient.readLine();

       // convert the client message into arguments
       clientArgs = convertMessage(clientMessage);

       // figures out what to do with the arguements
       serverResponse = parseArgs(clientArgs);

       // send the capitalized sentence back to the  client
       outToClient.writeBytes(serverResponse);

       // close the connection socket
       connectionSocket.close();
} catch(Exception e) {
  System.out.println("Error: " + e);
}
}

// converts a string to a list of arguments
public static ArrayList<String> convertMessage(String message) {
   ArrayList<String> args = new ArrayList<String>();
   System.out.println("Recieved From Client: " + message);
    if(message.length() == 0){
        return null;
    }
    String[] cont = message.split("#", 2); //split in two on hashtag for header info and message content
    String[] headerPieces = cont[0].split(" ", 2);
    args.add(headerPieces[0]);// add the number as the first argument to the list of arguments

    String[] intermediary;
    intermediary = cont[1].split(" "); // splits message by white space
    ArrayList<String> intermediary2  = new ArrayList<String>(Arrays.asList(intermediary));
    args.addAll(intermediary2);
    // now this has become an number, followed by a list of arguments
    return args;
}

// figure out what to do using a list of arguments
public static String parseArgs(ArrayList<String> args) {
  // if the list is empty
  if(args == null || args.size() == 0) {
    return "21 ERR#No arguments from Client";
  }
  // check if response code is response code for type,
  int responseCode = Integer.parseInt(args.get(0));
  String result = null;
  switch(responseCode){
    case 1: // put
      return "22 ERR#Cannot put a record before specifying type. run the type command. enter help for information";
    case 3: // get
      return "23 ERR#Cannot get a record before specifying type. run the type command. enter help for information";
    case 5: // del
      return "24 ERR#Cannot delete a record before specifying type. run the type command. enter help for information";
    case 7: // browse
      return "25 ERR#Cannot browse a database before specifying type. run the type command enter help for information";
    case 9: // type!!
      if(args.size() != 2){
        return "26 ERR#Wrong number of arguments for type";
      }
      result = getType(args.get(1));
      if(result.contentEquals("Type Not Found")){
        result = "20 ERR#"+result;
      } else {
        result = "10 PYT#" + result; // ack for type
      }
      return result;
    default:
      return "27 ERR#Invalid Argument";
  }
}

public static String getType(String type) {
  for(int i = 0; i < serverTypes.size(); i++) {
    if(serverTypes.get(i).trim().equalsIgnoreCase(type)) {
      return "" + serverPorts.get(i);
    }
  }
    return "Type Not Found";
  }
}

class ServerPrintThread extends Thread{
  BufferedReader inputStream = null;

  ServerPrintThread(BufferedReader input){
    inputStream = input;
    // accepts the inputStream
  }

  public void run(){
    while(true){
      try {
      String toPrint = new String();
      if(inputStream.ready()){
        toPrint = inputStream.readLine();
        System.out.println(toPrint);
      }
    } catch (Exception e){
      e.printStackTrace();
    }
    }

  }

}
