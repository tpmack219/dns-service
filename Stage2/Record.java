/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Terrell Mack, Shakeeb Saleh
 */
public class Record {
    private String Name;
    private String Value;
    private String Type;

    Record(){
    this.Name = null;
    this.Value = null;
    this.Type = null;
    }

    Record(String name, String value, String type){
        this.Name = name;
        this.Type = type;
        this.Value = value;
    }

    public String toString(){
        String result;
        result = this.getName() + " " + this.getValue() + " " + this.getType(); // remember to add a new line when saving it to file
        return result;
    }

    /**
     * @return the Name
     */
    public String getName() {
        return Name;
    }

    /**
     * @param Name the Name to set
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     * @return the Value
     */
    public String getValue() {
        return Value;
    }

    /**
     * @param Value the Value to set
     */
    public void setValue(String Value) {
        this.Value = Value;
    }

    /**
     * @return the Type
     */
    public String getType() {
        return Type;
    }

    /**
     * @param Type the Type to set
     */
    public void setType(String Type) {
        this.Type = Type;
    }

}
