/**
 *
 * @author Terrell Mack, Shakeeb Saleh
 */

import java.io.*; // Provides for system input and output through data
                  // streams, serialization and the file system
import java.net.*; // Provides the classes for implementing networking
                   // applications

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.lang.StringBuilder;
import java.util.regex.Pattern;

// TCP Client class
class ManagerClient {
        public static HashMap<String, String> verbs; //maps commands to verbs
        public static HashMap<String, Integer> numbers; //maps commands to numbers
        public static HashMap<Integer, String> acks; //maps acks
        public static boolean typeActive;

        public static void main(String argv[]) throws Exception {
                String userInput;
                String serverResponse;
                String serverAddress;
                int typeServerPort = -1;
                int serverPort;
                typeActive = false;
                initHashMaps();
                try {
                // create an input stream from the System.in
                BufferedReader inFromUser =
                new BufferedReader(new InputStreamReader(System.in));

                // if user didn't start the client with the address and port
                if(argv.length < 2) {
                  System.out.print("Enter Server Address: ");
                  serverAddress = inFromUser.readLine();

                  System.out.print("Enter Server Port: ");
                  serverPort = Integer.parseInt(inFromUser.readLine());

                } else {
                  // get the server port form command line
                  serverAddress = argv[0];
                  serverPort = Integer.parseInt(argv[1]);
                }

                System.out.println("DNS Client Ready:");
                while(true) {
                  //major while loop
                  System.out.print(">");

                  // read a line form the standard input
                  userInput = inFromUser.readLine();
                  ArrayList<String> typeArgs = checkType(userInput);
                  // this checks if the user was issuing a type command

                  if(userInput.trim().equalsIgnoreCase("help")) {
                    printHelp(); // help and exit are special cases
                  }

                  else if(userInput.trim().equalsIgnoreCase("exit")) {
                      System.out.println("Exiting..");
                      exit();
                    }
                  else if(userInput.trim().equalsIgnoreCase("done") && typeActive){
                      System.out.println("Ending session of current type");
                      typeActive = false;
                    }
                  else if(userInput.trim().equalsIgnoreCase("done") && (!typeActive)){
                    System.out.println("cannot issue 'done' without a type");
                  }

                  else if(typeActive) {
                    // type active means the user is connected to the server process--
                    // not through the manager itself, but after recieving certain input from the manager
                    Socket clientSocket = new Socket(serverAddress, typeServerPort);

                    // create an output stream from the socket output stream
                    DataOutputStream outToServer =
                    new DataOutputStream(clientSocket.getOutputStream());

                    // create an input stream from the socket input stream
                    BufferedReader inFromServer = new BufferedReader(
                    new InputStreamReader(clientSocket.getInputStream()));

                    // send the sentence read to the server
                    userInput = replaceCommand(userInput);
                    if (userInput == null){
                      // skip everything and go to the top of the while loop
                      continue;
                    }
                    outToServer.writeBytes(userInput + '\n');

                    // get the reply from the server
                    serverResponse = inFromServer.readLine();
                    serverResponse = recieveResponse(serverResponse);
                    //recieveResponse truncates out the header information
                    // print the returned sentence
                    System.out.println("FROM SERVER: " + serverResponse);

                    // close the socket
                    clientSocket.close();

                  }
                  else if(typeArgs != null && typeActive == false) {
                    // this means that the user is currently connected to a manager process
                    // and is currently trying to obtain a type
                    Socket clientSocket = new Socket(serverAddress, serverPort);

                    // create an output stream from the socket output stream
                    DataOutputStream outToServer =
                    new DataOutputStream(clientSocket.getOutputStream());

                    // create an input stream from the socket input stream
                    BufferedReader inFromServer = new BufferedReader(
                    new InputStreamReader(clientSocket.getInputStream()));

                    // send the sentence read to the server
                    userInput = replaceCommand(userInput);
                    if(userInput == null){
                      // jump to top
                      continue;
                    }
                    outToServer.writeBytes(userInput + '\n');

                    // get the reply from the server
                    serverResponse = inFromServer.readLine();
                    System.out.println("<"+serverResponse+">");
                    serverResponse = recieveResponse(serverResponse);

                      if(serverResponse.contentEquals("Type Not Found")) {
                        System.out.println(serverResponse);
                      }

                      else if(serverResponse.contentEquals("Wrong number of arguments for type")){
                        System.out.println(serverResponse);
                      }

                      else {
                        typeActive = true;
                        typeServerPort = Integer.parseInt(serverResponse);

                        System.out.print(">");
                      }

                    // close the socket
                    clientSocket.close();
                  }

                  else {
                  // elsewise the user doesnt have a type yet, so it's still only connected to the manager
                  Socket clientSocket = new Socket(serverAddress, serverPort);

                  // create an output stream from the socket output stream
                  DataOutputStream outToServer =
                  new DataOutputStream(clientSocket.getOutputStream());

                  // create an input stream from the socket input stream
                  BufferedReader inFromServer = new BufferedReader(
                  new InputStreamReader(clientSocket.getInputStream()));

                  // send the sentence read to the server
                  userInput = replaceCommand(userInput);
                  if(userInput == null){
                    continue;
                  }
                  outToServer.writeBytes(userInput + '\n');

                  // get the reply from the server
                  serverResponse = inFromServer.readLine();
                  serverResponse = recieveResponse(serverResponse);

                  // print the returned sentence
                  System.out.println("FROM SERVER: " + serverResponse);

                  // close the socket
                  clientSocket.close();
                }
          } // end of major while loop
        } catch (Exception e){
          e.printStackTrace();
        }

      }

      public static ArrayList<String> checkType(String s) {
          ArrayList<String> args = new ArrayList<String>();
          if(s.length() == 0){
              return null;
          }
          String[] intermediary;
          intermediary = s.split(" "); // splits message by white space
          args = new ArrayList<String>(Arrays.asList(intermediary));

          if(args != null || args.size() == 2) {
            if(args.get(0).equalsIgnoreCase("type")) {
              return args;
            }
          }
          return null;
      }

        // prints the help menu TODO: this needs a different help
        public static void printHelp() {
            System.out.println("help stuff goes here");
            System.out.println("list of commands: ");
            System.out.println("help");
            System.out.println("    prints out the help menu");
            System.out.println("put <name> <value> ");
            System.out.println("    places a record into the database");
            System.out.println("get <name> ");
            System.out.println("    returns a record from the database");
            System.out.println("del <name> ");
            System.out.println("    deletes a record from the database");
            System.out.println("browse ");
            System.out.println("    prints out the whole database");
            System.out.println("exit ");
            System.out.println("    exits the client");
            System.out.println("type <type>");
            System.out.println("    selects a type of name database to read from");
            System.out.println("done ");
            System.out.println("    ends the session with the server of that type ");
        }

        // ends the client program
        public static void exit() {
            //if we need to do more before exiting do it here
            System.exit(0);
        }

        public static void initHashMaps(){
          // basically this initializes the protocol, numbers and verbs
          verbs = new HashMap<String, String>();
          verbs.put("put","PUT");
          verbs.put("get","GET");
          verbs.put("del","DEL");
          verbs.put("browse","BRO");
          verbs.put("type","TYP");
          verbs.put("done","DON");
          // verbs maps commands to verbs
          numbers = new HashMap<String, Integer>();
          numbers.put("put",1); //ack is 2
          numbers.put("get",3); //ack is 4
          numbers.put("del",5); //ack is 6
          numbers.put("browse",7); //ack is 8
          numbers.put("type",9); // ack is 10
          numbers.put("done",11); // ack is 12
          // numbers maps commands to status numbers
          acks = new HashMap<Integer, String>();
          acks.put(2,"TUP");
          acks.put(4,"TEG");
          acks.put(6,"LED");
          acks.put(8,"ORB");
          acks.put(10,"PYT");
          acks.put(12,"NOD");
          //acks maps ack numbers to ack verbs

        }

        public static String replaceCommand(String userInput){
          // before sending the sentence over, split on whitespace for two strings,
          // and then append a #, the verb, and then the number to the front of the string
          String[] cont = userInput.split(" ", 2); //splits it in two on the first space
          if(userInput.equals("")){
            return null;
          }
          if(numbers.get(cont[0].toLowerCase()) == null){
            System.out.println("not a supported command");
            return null;
          }
          String prefix = numbers.get(cont[0].toLowerCase()) + " " + verbs.get(cont[0].toLowerCase()) + "#";
          System.out.println("Response Code:" + numbers.get(cont[0].toLowerCase())); // first the number
          if (cont.length == 2){
            prefix = prefix + cont[1];
          } else {
            // nothing
          }
          System.out.println("Sending Message:"+ prefix );
          return prefix;
        }

        public static String recieveResponse(String serverResponse){
          String[] cont = serverResponse.split("#", 2); // split the user message in two
          String[] headerPieces = cont[0].split(" ", 2); // split the header itself in two
          int responseCode = Integer.parseInt(headerPieces[0]); // get the responseCode
          String result;
          switch(responseCode){ // switching on the number
            case 2: // acknowledging put
              System.out.println("put acknowledged!");
              result = cont[1]; // the rest of the string
              break;
            case 4: // acking get
              System.out.println("get command acknowledged!");
              result = cont[1];
              break;
            case 6: //acking del
              System.out.println("del command acknowledged!");
              result = cont[1];
              break;
            case 8: //acking browse
              System.out.println("browse command acknowledged!");
              // print out the browse properly formatted
              printBrowse(cont[1]);
              result = " ";
              break;
            case 10://acking type
              System.out.println("type command acknowledged!");
              result = cont[1];
              break;
            case 12: //acking done
              System.out.println("done command acknowledged!");
              typeActive = false; // this will switch it back to the manager port
              result = cont[1];
              break;
            default: // it's an error message
              System.out.println("error recieved!");
              result = cont[1];
              break;
          }
          return result;

        }

        public static void printBrowse(String item){
          ArrayList<String> browseArray = new ArrayList<String>(Arrays.asList(item.split(Pattern.quote("+"))));
          if(item == null){
            System.out.println("empty database");
            return;
          }
          for(String s : browseArray){
            System.out.println(s);
          }
          return;
        }
}
