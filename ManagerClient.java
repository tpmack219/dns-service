/**
 *
 * @author Terrell Mack, Shakeeb Saleh
 */

import java.io.*; // Provides for system input and output through data
                  // streams, serialization and the file system
import java.net.*; // Provides the classes for implementing networking
                   // applications

import java.util.ArrayList;
import java.util.Arrays;

// TCP Client class
class ManagerClient {

        public static void main(String argv[]) throws Exception {
                String userInput;
                String serverResponse;
                String serverAddress;
                int typeServerPort = -1;
                int serverPort;
                boolean typeActive = false;

                // create an input stream from the System.in
                BufferedReader inFromUser =
                new BufferedReader(new InputStreamReader(System.in));

                // if user didn't start the client with the address and port
                if(argv.length < 2) {
                  System.out.print("Enter Server Address: ");
                  serverAddress = inFromUser.readLine();

                  System.out.print("Enter Server Port: ");
                  serverPort = Integer.parseInt(inFromUser.readLine());

                } else {
                  // get the server port form command line
                  serverAddress = argv[0];
                  serverPort = Integer.parseInt(argv[1]);
                }

                System.out.println("DNS Client Ready:");
                while(true) {
                System.out.print(">");

                // read a line form the standard input
                userInput = inFromUser.readLine();
                ArrayList<String> typeArgs = checkType(userInput);

                if(userInput.trim().equalsIgnoreCase("help")) {
                  printHelp();
                }

                else if(userInput.trim().equalsIgnoreCase("exit")) {
                    System.out.println("Exiting..");
                    exit();
                  }

                else if(typeActive) {
                  Socket clientSocket = new Socket(serverAddress, typeServerPort);

                  // create an output stream from the socket output stream
                  DataOutputStream outToServer =
                  new DataOutputStream(clientSocket.getOutputStream());

                  // create an input stream from the socket input stream
                  BufferedReader inFromServer = new BufferedReader(
                  new InputStreamReader(clientSocket.getInputStream()));

                  // send the sentence read to the server
                  outToServer.writeBytes(userInput + '\n');

                  // get the reply from the server
                  serverResponse = inFromServer.readLine();

                  // print the returned sentence
                  System.out.println("FROM SERVER: " + serverResponse);

                  // close the socket
                  clientSocket.close();

                }
                else if(typeArgs != null && typeActive == false) {

                  Socket clientSocket = new Socket(serverAddress, serverPort);

                  // create an output stream from the socket output stream
                  DataOutputStream outToServer =
                  new DataOutputStream(clientSocket.getOutputStream());

                  // create an input stream from the socket input stream
                  BufferedReader inFromServer = new BufferedReader(
                  new InputStreamReader(clientSocket.getInputStream()));

                  // send the sentence read to the server
                  outToServer.writeBytes(userInput + '\n');

                  // get the reply from the server
                  serverResponse = inFromServer.readLine();

                    if(serverResponse.contentEquals("Type Not Found")) {
                      System.out.println(serverResponse);
                    }

                    else {
                      typeActive = true;
                      typeServerPort = Integer.parseInt(serverResponse);

                      System.out.print(">");
                    }

                  // close the socket
                  clientSocket.close();
                }

                else {
                Socket clientSocket = new Socket(serverAddress, serverPort);

                // create an output stream from the socket output stream
                DataOutputStream outToServer =
                new DataOutputStream(clientSocket.getOutputStream());

                // create an input stream from the socket input stream
                BufferedReader inFromServer = new BufferedReader(
                new InputStreamReader(clientSocket.getInputStream()));

                // send the sentence read to the server
                outToServer.writeBytes(userInput + '\n');

                // get the reply from the server
                serverResponse = inFromServer.readLine();

                // print the returned sentence
                System.out.println("FROM SERVER: " + serverResponse);

                // close the socket
                clientSocket.close();
              }
        }
      }

      public static ArrayList<String> checkType(String s) {
          ArrayList<String> args = new ArrayList<String>();
          if(s.length() == 0){
              return null;
          }
          String[] intermediary;
          intermediary = s.split(" "); // splits message by white space
          args = new ArrayList<String>(Arrays.asList(intermediary));

          if(args != null || args.size() == 2) {
            if(args.get(0).equalsIgnoreCase("type")) {
              return args;
            }
          }
          return null;
      }

        // prints the help menu TODO: this needs a different help
        public static void printHelp() {
            System.out.println("help stuff goes here");
            System.out.println("list of commands: ");
            System.out.println("help");
            System.out.println("    prints out the help menu");
            System.out.println("put <name> <value> <type>");
            System.out.println("    places a record into the database");
            System.out.println("get <name> <type> ");
            System.out.println("    returns a record from the database");
            System.out.println("del <name> <type> ");
            System.out.println("    deletes a record from the database");
            System.out.println("browse ");
            System.out.println("    prints out the whole database");
            System.out.println("exit ");
            System.out.println("    exits the client");
        }

        // ends the client program
        public static void exit() {
            //if we need to do more before exiting do it here
            System.exit(0);
        }
}
