/**
 *
 * @author Terrell Mack, Shakeeb Saleh
 */

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Arrays;

// TCP Server Class
class Manager {

  private static Socket connectionSocket;
  private static String clientMessage;
  private static String serverResponse;
  private static boolean serversStarted = false;
  private static ArrayList<String> clientArgs;

  private static ArrayList<String> serverTypes;
  private static ArrayList<Integer> serverPorts;

  private static String typesFilePath = "manager.in";
  static int wellKnownPort = 4132;

  public static void main(String[] args) {
    // generate servers types based on type file
    loadServersFrom(typesFilePath);

    // startup all the servers
    startServers();

    try {
      // create a server socket (TCP)
      ServerSocket welcomeSocket = new ServerSocket(wellKnownPort);

      while(true) {
        // Wait and accept client connection
        Socket connectionSocket = welcomeSocket.accept();

        // Each manager gets its own thread so multiple can run at once
        ConnectionThread ct = new ConnectionThread(connectionSocket, serverTypes, serverPorts);
        ct.start();
    }
  } catch(IOException e) {
    System.out.print("Error: " + e);
  }}

  // starts up Type Servers, each on their own process
  public static void startServers() {
    serverPorts = new ArrayList<Integer>();
    if(serversStarted == false) {
      // Each server gets its own process
      for(String s : serverTypes) {
        try {
        ProcessBuilder pb = new ProcessBuilder("java","DNSTypeServer", s);
        Process proc = pb.start();
        BufferedReader br = new BufferedReader(new InputStreamReader(proc.getInputStream()));
        String portNumber = br.readLine();
        serverPorts.add(Integer.parseInt(portNumber));

      } catch(Exception e) {
        System.out.println("Error: " + e);
      }

      }
    }
  }

  // this prints out all the type servers with the port numbers they are running on
  public static void serverInfo() {

    System.out.println("Server Information: ");
    for(int i = 0; i < serverTypes.size(); i++) {
      System.out.print(serverTypes.get(i));
      System.out.print(" " + serverPorts.get(i) + "\n");
    }

  }

  public static void loadServersFrom(String filename) {
    serverTypes = new ArrayList<String>();
    try {
    BufferedReader br = new BufferedReader(new FileReader(filename));
    String type = br.readLine();
    while (type != null) {
        serverTypes.add(type);
        type = br.readLine();
    }
    br.close();
    } catch(Exception e) {
      System.out.println("Error:" + e);
    }
  }
}

class ConnectionThread extends Thread {

  private static Socket connectionSocket;
  private static String clientMessage;
  private static String serverResponse;
  private static boolean serversStarted = false;
  private static ArrayList<String> clientArgs;

  private static ArrayList<String> serverTypes;
  private static ArrayList<Integer> serverPorts;

  ConnectionThread(Socket connSock, ArrayList<String> types, ArrayList<Integer> ports) {
    connectionSocket = connSock;
    serverTypes = types;
    serverPorts = ports;
  }

  // handles the client request
   public void run () {
     try {
       //create an input stream from the socket input stream
       BufferedReader inFromClient = new BufferedReader(
          new InputStreamReader(connectionSocket.getInputStream()));

       // create an output stream from the socket output stream
       DataOutputStream outToClient =
       new DataOutputStream(connectionSocket.getOutputStream());

       // read a line form the input stream
       clientMessage = inFromClient.readLine();

       // convert the client message into arguments
       clientArgs = convertMessage(clientMessage);

       // figures out what to do with the arguements
       serverResponse = parseArgs(clientArgs);

       // send the capitalized sentence back to the  client
       outToClient.writeBytes(serverResponse);

       // close the connection socket
       connectionSocket.close();
} catch(Exception e) {
  System.out.println("Error: " + e);
}
}

// converts a string to a list of arguments
public static ArrayList<String> convertMessage(String message) {
    ArrayList<String> args = new ArrayList<String>();
    if(message.length() == 0){
        return args;
    }
    String[] intermediary;
    intermediary = message.split(" "); // splits message by white space
    args = new ArrayList<String>(Arrays.asList(intermediary));
    return args;
}

// figure out what to do using a list of arguments
public static String parseArgs(ArrayList<String> args) {
  // if the list is empty
  if(args == null || args.size() == 0) {
    return "No arguments from Client";
  }
  // put argument
  if(args.get(0).equalsIgnoreCase("type")) {
    if(args.size() != 2)
      return "Wrong number of arguments for type";
    return getType(args.get(1));
  }
  else {
  return "Invalid Argument";
  }
}

public static String getType(String type) {
  for(int i = 0; i < serverTypes.size(); i++) {
    if(serverTypes.get(i).trim().equalsIgnoreCase(type)) {
      return "" + serverPorts.get(i);
    }

  }
    return "Type Not Found";
  }

}
