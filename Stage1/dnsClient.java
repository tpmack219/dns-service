/**
 *
 * @author Terrell Mack, Shakeeb Saleh
 */

import java.io.*; // Provides for system input and output through data
                  // streams, serialization and the file system
import java.net.*; // Provides the classes for implementing networking
                   // applications
import java.util.*;
import java.util.regex.Pattern;

// TCP Client class
class dnsClient {
  public static HashMap<String, String> verbs; //maps commands to verbs
  public static HashMap<String, Integer> numbers; //maps commands to numbers
  public static HashMap<Integer, String> acks; //maps acks

        public static void main(String argv[]) throws Exception {
                String userInput;
                String serverResponse;
                String serverAddress;
                int serverPort;
                // create an input stream from the System.in
                BufferedReader inFromUser =
                new BufferedReader(new InputStreamReader(System.in));
                initHashMaps();

                // if user didn't start the client with the address and port
                try{
                if(argv.length < 2) {
                  System.out.print("Enter Server Address: ");
                  serverAddress = inFromUser.readLine();

                  System.out.print("Enter Server Port: ");
                  serverPort = Integer.parseInt(inFromUser.readLine());

                } else {
                  // get the server port form command line
                  serverAddress = argv[0];
                  serverPort = Integer.parseInt(argv[1]);
                }

                System.out.println("DNS Client Ready:");
                while(true) {
                System.out.print(">");


                // read a line form the standard input
                userInput = inFromUser.readLine();

                if(userInput.trim().equalsIgnoreCase("help")) {
                  printHelp();
                }

                else if(userInput.trim().equalsIgnoreCase("exit")) {
                  System.out.println("Exiting..");
                  exit();
                }

                else {
                Socket clientSocket = new Socket(serverAddress, serverPort);

                // create an output stream from the socket output stream
                DataOutputStream outToServer =
                new DataOutputStream(clientSocket.getOutputStream());

                // create an input stream from the socket input stream
                BufferedReader inFromServer = new BufferedReader(
                new InputStreamReader(clientSocket.getInputStream()));

                // send the sentence read to the server
                userInput = replaceCommand(userInput);
                if(userInput == null){
                  continue;
                }
                outToServer.writeBytes(userInput + '\n');


                // get the reply from the server
                serverResponse = inFromServer.readLine();
                serverResponse = recieveResponse(serverResponse);

                // print the returned sentence
                System.out.println("FROM SERVER: " + serverResponse);

                  // close the socket
                  clientSocket.close();
              }
        } // end major while loop
      } catch (Exception e) {
        e.printStackTrace();
      }
      }

        // prints the help menu
        public static void printHelp() {
            System.out.println("help stuff goes here");
            System.out.println("list of commands: ");
            System.out.println("help");
            System.out.println("    prints out the help menu");
            System.out.println("put <name> <value> <type>");
            System.out.println("    places a record into the database");
            System.out.println("get <name> <type> ");
            System.out.println("    returns a record from the database");
            System.out.println("del <name> <type> ");
            System.out.println("    deletes a record from the database");
            System.out.println("browse ");
            System.out.println("    prints out the whole database");
            System.out.println("exit ");
            System.out.println("    exits the client");
        }

        // ends the client program
        public static void exit() {
            //if we need to do more before exiting do it here
            System.exit(0);
        }

        public static String replaceCommand(String userInput){
          // before sending the sentence over, split on whitespace for two strings,
          // and then append a #, the verb, and then the number to the front of the string
          String[] cont = userInput.split(" ", 2); //splits it in two on the first space
          if(userInput.equals("")){
            return null;
          }
          if(numbers.get(cont[0].toLowerCase()) == null){
            System.out.println("not a supported command");
            return null;
          }
          String prefix = numbers.get(cont[0].toLowerCase()) + " " + verbs.get(cont[0].toLowerCase()) + "#";
          System.out.println("Response Code:" + numbers.get(cont[0].toLowerCase())); // first the number
          if (cont.length == 2){ // why do i do this?
            prefix = prefix + cont[1];
          } else {
            // nothing
          }
          System.out.println("Sending Message:"+ prefix);
          return prefix;
        }

        public static String recieveResponse(String serverResponse){
          String[] cont = serverResponse.split("#", 2); // split the user message in two
          if(serverResponse.equals("")){
            return null;
          }
          String[] headerPieces = cont[0].split(" ", 2); // split the header itself in two
          int responseCode = Integer.parseInt(headerPieces[0]); // get the responseCode
          String result;
          switch(responseCode){ // switching on the number
            case 2: // acknowledging put
              System.out.println("put acknowledged!");
              result = cont[1]; // the rest of the string
              break;
            case 4: // acking get
              System.out.println("get command acknowledged!");
              result = cont[1];
              break;
            case 6: //acking del
              System.out.println("del command acknowledged!");
              result = cont[1];
              break;
            case 8: //acking browse
              System.out.println("browse command acknowledged!");
              // print out the browse properly formatted
              printBrowse(cont[1]);
              result = cont[1];
              break;
            default: // it's an error message
              System.out.println("error recieved!");
              result = cont[1];
              break;
          }
          return result;

        }

        public static void printBrowse(String item){
          // this just takes the format of browse and prints it in a readable way
          ArrayList<String> browseArray = new ArrayList<String>(Arrays.asList(item.split(Pattern.quote("+"))));
          if(item == null){
            System.out.println("empty database");
            return;
          }
          for(String s : browseArray){
            System.out.println(s);
          }
          return;
        }

        public static void initHashMaps(){
          // basically this initializes the protocol, numbers and verbs
          verbs = new HashMap<String, String>();
          verbs.put("put","PUT");
          verbs.put("get","GET");
          verbs.put("del","DEL");
          verbs.put("browse","BRO");
          verbs.put("type","TYP");
          verbs.put("done","DON");
          // verbs maps commands to verbs
          numbers = new HashMap<String, Integer>();
          numbers.put("put",1); //ack is 2
          numbers.put("get",3); //ack is 4
          numbers.put("del",5); //ack is 6
          numbers.put("browse",7); //ack is 8
          numbers.put("type",9); // ack is 10
          numbers.put("done",11); // ack is 12
          // numbers maps commands to status numbers
          acks = new HashMap<Integer, String>();
          acks.put(2,"TUP");
          acks.put(4,"TEG");
          acks.put(6,"LED");
          acks.put(8,"ORB");
          acks.put(10,"PYT");
          acks.put(12,"NOD");
          //acks maps ack numbers to ack verbs

        }
}
