/**
* DNS Server
* CSE 310 - Fall 2015
* @author Terrell Mack, Shakeeb Saleh
 */

import java.io.*; // Provides for system input and output through data
                  // streams, serialization and the file system
import java.net.*; // Provides the classes for implementing networking
                   // applications

import java.util.ArrayList;
import java.util.Arrays;

// TCP Server Class
class dnsServer {

      private static ArrayList<Record> records; //holds database of records

        public static void main(String argv[]) throws Exception {
          records = new ArrayList<Record>();
                // TODO: remove (FOR DEBUGGING)
                // get the port number assigned from the command line
                //int lisPort = Integer.parseInt(argv[0]);
                //ServerSocket welcomeSocket = new ServerSocket(lisPort);

                // create a server socket (TCP) eph
                ServerSocket welcomeSocket = new ServerSocket(0);

                System.out.print("\nDNS server ready on port " + welcomeSocket.getLocalPort());

                // loop infinitely (process clients sequentially)
              while(true) {
                        // Wait and accept client connection
                        Socket connectionSocket = welcomeSocket.accept();

                        // Each connection gets its own thread so multiple can run at once
                        ClientThread ct = new ClientThread(connectionSocket, records);
                        ct.start();
                }
        }
}

class ClientThread extends Thread {

  private static ArrayList<Record> records;
  Socket connectionSocket;
  String clientMessage;
  String serverResponse;
  ArrayList<String> clientArgs;

  ClientThread(Socket connSock, ArrayList<Record> recs) {
    connectionSocket = connSock;
    records = recs;
  }

  // handles the client request
   public void run () {
     try {
     //create an input stream from the socket input stream
     BufferedReader inFromClient = new BufferedReader(
        new InputStreamReader(connectionSocket.getInputStream()));

     // create an output stream from the socket output stream
     DataOutputStream outToClient =
     new DataOutputStream(connectionSocket.getOutputStream());

     // read a line form the input stream
     clientMessage = inFromClient.readLine();

     // convert the client message into arguments
     clientArgs = convertMessage(clientMessage);

     // figures out what to do with the arguements
     serverResponse = parseArgs(clientArgs);

     // send the capitalized sentence back to the  client
     outToClient.writeBytes(serverResponse +"\n");

     // close the connection socket
     connectionSocket.close();
   } catch(IOException e) {
     System.out.print("Error: " + e);
   }
   }

   // converts a string to a list of arguments
   public static ArrayList<String> convertMessage(String message) {
      ArrayList<String> args = new ArrayList<String>();
      System.out.println("Recieved from Client:"+ message);
       if(message == null){
         return null;
       }
       if(message.length() == 0){
           return args;
       }
       String[] cont = message.split("#", 2); //split in two on hashtag for header info and message content
       String[] headerPieces = cont[0].split(" ", 2);
       args.add(headerPieces[0]);// add the number as the first argument to the list of arguments

       String[] intermediary;
       intermediary = cont[1].split(" "); // splits message by white space
       ArrayList<String> intermediary2  = new ArrayList<String>(Arrays.asList(intermediary));
       args.addAll(intermediary2);
       // now this has become an number, followed by a list of arguments
       System.out.println(args.toString());
       return args;
   }

   // figure out what to do using a list of arguments
   public static String parseArgs(ArrayList<String> args) {

     // if the list is empty
     if(args == null || args.size() == 0) {
       return "21 ERR#No arguments from Client";
     }

     System.out.println(args.get(0));
     int responseCode = Integer.parseInt(args.get(0)); //get the responseCode from the args,
     // the rest of the args are treated the same way as they were before
     String result = "";
     System.out.println("parsing args");

     switch(responseCode){
       case 1: // put
          System.out.println("Response Code:"+ responseCode);
          if(args.size() != 4){ // put <name> <value> <type>
            System.out.println("wrong number of arguments");
            result = "Wrong number of arguments for put"; // error code
            // add on header error info
            result = "13 ERR#" + result;
            System.out.println("Sending to Client:"+result);
          } else {
            System.out.println("correct number of arguments");
            result = put(args.get(1), args.get(2), args.get(3));
            // add on header info
            result = "2 TUP#" + result;
            System.out.println("Sending to Client:"+result);
          }
          return result;
       case 3: // get
          System.out.println("Response Code:"+ responseCode);
          if(args.size() != 3){ //get <name> <type>
            result = "Wrong number of arguments for get"; // error code
            result = "14 ERR#" + result;
            System.out.println("Sending to Client:"+result);
            return result;
          }
          Record rec = get(args.get(1), args.get(2));
          if(rec == null){
            result = "Record Not Found"; // error code
            result = "15 ERR#" + result;
            System.out.println("Sending to Client:"+result);
          } else {
            result = rec.toString();
            result = "4 TEG#" + result;
            System.out.println("Sending to Client:"+result);
          }
          return result;
       case 5: // del
          System.out.println("Response Code:"+ responseCode);
          if(args.size() != 3){ // del <name> <type>
              result = "Wrong number of arguments for del";
              result = "16 ERR#" + result;
              System.out.println("Sending to Client:"+result);
              return result;
            } else {
              result = del(args.get(1), args.get(2));
              // header for DEL is handled in the function itself
              System.out.println("Sending to Client:"+result);
              return result;
            }
       case 7: // browse
          System.out.println("Response Code:"+ responseCode);
          result = browse();
          if(result == null){
            result = "Records are Empty";
            result = "18 ERR#" + result;
            System.out.println("Sending to Client:"+result);
          } else {
            result = "8 ORB#" + result;
            System.out.println("Sending to Client:"+result);
          }
          return result;
      default: // shouldn't be able to get here
          System.out.println("Response Code:"+ responseCode);
          result = "20 ERR# default";
          System.out.println("Sending to Client:"+result);
          return result;

     }
   }

   // adds a new record from the records database
   public static String put(String name, String value, String type){
       if(!(type.equalsIgnoreCase("NS")
               || type.equalsIgnoreCase("A"))){ // add other types when neccessary
           return "Invalid Type for Put: " + type;
       }

       if(findRecord(name, type) != null){
         Record r = findRecord(name, type);
         r.setValue(value);
         return "Succesfully modified Record " + name;
       }
       Record record = new Record(name, value, type);
       records.add(record);
       return "Succesfully Added Record " + value;
   }

   // gets a record from the records database
   public static Record get(String name, String type){
       // if there arent exactly 3 arguments, its wrong
       // first argument is get itself
       // must be in order name, type

       // returns a Record with that name and type
       // search by type, and then query name
       Record result = findRecord(name, type);
       if(result == null){
           System.out.println("17 ERR#record of name:" + name + "and Type" + type + "does not exist.");
           return null;
       }
       return result;
   }

   // deletes a record from the record database
   public static String del(String name, String type) {
       Record result = findRecord(name, type);
       if(result == null){
           return "17 ERR#record of name:" + name + "and Type" + type + "does not exist.";
       } else {
           records.remove(result);
           return "6 LED#Record Deleted Succesfully";
       }
   }

   // returns all records from the record databse
   public static String browse() {
       if(records.isEmpty()){
           return "Database is empty";
       } else {
           //iterate through database,
           // putting everything into a massive string
           // and making sure there are endls after each record
         String allRecords = new String();
           for(Record r: records){
               allRecords += r.toString() + "+";
           }
           return allRecords;
       }
   }

   // find a particular record from the record database
   public static Record findRecord(String name, String type) {
     for(Record r : records){
         if(r.getType().equals(type)){
             if(r.getName().equals(name)){
                 return r;
             }
         }
     }
     return null;
   }
}
